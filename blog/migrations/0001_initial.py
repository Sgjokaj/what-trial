# Generated by Django 3.2 on 2021-11-19 23:51

import ckeditor_uploader.fields
from django.db import migrations, models
import django.utils.timezone
import taggit.managers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('taggit', '0003_taggeditem_add_unique_index'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('images', models.ImageField(upload_to='imgs/')),
                ('slug', models.SlugField(default='', editable=False, max_length=500)),
                ('content', ckeditor_uploader.fields.RichTextUploadingField()),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('taggit', taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags')),
            ],
            options={
                'ordering': ['-creation_date'],
            },
        ),
    ]

from django.urls import path
from .views import (
    PostListView,
    PostDetailView,
    TagIndexView,

)
from . import views


urlpatterns = [
    path('', PostListView.as_view(), name='blog-home'),
    path('post/<slug:slug>/', PostDetailView.as_view(), name='post-detail'),
    path('tags/<slug:tag_slug>/', TagIndexView.as_view(), name='posts_by_tag'),
]